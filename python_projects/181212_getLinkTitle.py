import requests
from bs4 import BeautifulSoup

# start range(1054140000,1054140100)
for i in range(1054131100,1054131200):
	url = "https://m.study.163.com/article/" + str(i)
	res = requests.get(url)
	# 使用 UTF-8 编码
	res.encoding = 'utf-8'
	# 使用剖析器为 lxml
	soup = BeautifulSoup(res.text, 'lxml')
	print(str(i) + ": " + soup.title.text)
