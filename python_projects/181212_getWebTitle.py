from urllib.request import urlopen
from urllib.error import HTTPError, URLError
from bs4 import BeautifulSoup
import ssl
import urllib
 
ssl._create_default_https_context = ssl._create_unverified_context


def dump(sth):
	print(str(sth))

def getTitle(url):
    try:
        html = urlopen(url)
    except (HTTPError, URLError) as e:
        return None
    try:
        bsObj = BeautifulSoup(html.read(), 'lxml')
        titleTarget = bsObj.title.text
    except AttributeError as e:
        return None
    return titleTarget


titleTarget = getTitle("https://m.study.163.com/article/1054140278")
if titleTarget == None:
    print("Title could not be found!")
else:
    print(titleTarget)
