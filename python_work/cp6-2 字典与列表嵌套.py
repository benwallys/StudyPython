# -- coding: utf-8 --
# 字典

# 嵌套：将一系列字典存储在列表中，或将列表作为值存储在字典中
# 在列表中嵌套字典、在字典中嵌套列表、在字典中嵌套字典 等

# 1.字典组成列表
alien_0 = {'color': 'blue', 'points': 5}
alien_1 = {'color': 'red', 'points': 15}
alien_2 = {'color': 'green', 'points': 25}

aliens = [alien_0, alien_1, alien_2]

print(aliens)
print(len(aliens))

# 2.字典中存储列表
favorite_languages = {
	'jen': ['python', 'C'],
	'sarah': ['ruby'],
	'edward': ['go'],
	'phil': ['php', 'python']
}
for name,languages in favorite_languages.items():
	if len(languages) == 1:
		print('\n' + name.title() + "'s favorite language is: ")
		print('\t' + languages[0])
	else:
		print('\n' + name.title() + "'s favorite languages are: ")
		for language in languages:
			print('\t' + language)

# 3.在字典中存储字典
users = {
	'harrison': {
		'first': 'hecong',
		'last': 'li',
		'from': 'zhaoqing',
	},
	'steven': {
		'first': 'yupu',
		'last': 'bao',
		'from': 'xiaan',
	},
	'mavis': {
		'first': 'xiaomei',
		'last': 'wu',
		'from': 'jiangmen',
	}
}
for name,details in users.items():
	print('\nUsername: ' + name.title())
	fullname = details['first'].title() + '.' + details['last'].title()
	print('\tFullname: ' + fullname)
	print('\tFrom: ' + details['from'].title())