# -- coding: utf-8 --
# 类的继承

# 先创建一个父类
## 汽车类，属性有厂商、型号、年费、里程、油箱，方法有展示车名、读取里程值、加油描述
class Car():
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        self.km = 0
        self.tank = 60

    def show_the_car(self):
        long_name = str(self.year) + ' ' + self.make + ' ' + self.model
        print('\nThis is ' + long_name.title())

    def read_km(self):
        print('This car has ran ' + str(self.km) + ' kilometers yet.')

    def fill_gas_tank(self):
        print('Please fill the gas of ' + str(self.tank) + 'L')


## 建立一个父类的实例
my_car = Car('Benz', 'S400', 2016)
my_car.show_the_car()
my_car.read_km()
my_car.fill_gas_tank()


## 然后创建一个子类，电动车继承父类车的属性和方法
## 电动车自己的属性有电池容量，自己的方法有读取电量、特殊的加油提示
class ElectricCar(Car):
    def __init__(self, make, model, year, battery=150):
        '''电动汽车的独特之处 初始化父类的属性，再初始化电动汽车特有的属性'''
        super().__init__(make, model, year)
        # 定义子类的特有属性
        self.battery = battery

    # 定义子类的特有方法
    def read_battery(self):
        print('This car has a ' + str(self.battery) + '-KWh battery.')

    # 重写父类的 fill_gas_tank() 方法
    def fill_gas_tank(self):
        print('My ElectricCar has no tank, but a ' + str(self.battery) + '-KWh battery.')

## 建立一个子类的实例
my_tesla = ElectricCar('Tesla', 'Model S', 2018)
my_tesla.show_the_car()
my_tesla.read_km()
my_tesla.read_battery()
my_tesla.fill_gas_tank()



# 将实例用作属性
##################################################
## 使用代码模拟实物时，你可能会发现自己给类添加的细节越来越多：
## 属性和方法清单以及文件都越来越长。在这种情况下，可能需要将
## 类的一部分作为一个独立的类提取出来。你可以将大型类拆分成多
## 个协同工作的小类。
##################################################
## 例如，新建一个 GasCar 类，不断给 GasCar 类添加细节时，
## 我们可能会发现其中包含很多专门针对汽车气罐的属性和方法。
## 在这种情况下，我们可将这些属性和方法提取出来，放到另一个名
## 为 Tank 的类中，并将一个 Tank 实例用作 GasCar 类的一个属性:
##################################################
class Tank():
    """一次模拟气罐车的尝试"""
    def __init__(self, tank=80):
        self.tank = tank

    def show_tank(self):
        print('The tank of the gascar is ' + str(self.tank))

class GasCar(Car):
    """气罐车的独特之处"""
    def __init__(self, make, model, year):
        # 下面这行初始化父类属性
        super().__init__(make, model, year)
        # 下面这行直接将 Tank 的实例作为 GasCar 的一个初始化属性
        self.gas = Tank()

## 建立一个 GasCar 实例
my_npg = GasCar('YuTong', 'Bus700', 2017)
## 下面这行调用父类的方法
my_npg.show_the_car()
## 下面两行访问实例中关于 gas（Tank）属性的属性（tank值）和方法（show_tank）
print(my_npg.gas.tank)
my_npg.gas.show_tank()
