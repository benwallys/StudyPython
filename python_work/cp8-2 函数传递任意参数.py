# -- coding: utf-8 --
# 函数
def p():
    print('\n')

### 传递任意数量的实参
### 参数中的形参名使用【*parameter】，意思是创建一个空元组，并将收到的所有值都封装到这个元组中
p()
def make_pizza(*toppings):
    print('制作 pizza 所需要的材料有：')
    for x in toppings:
        print('- ' + x)

make_pizza('cheese', 'mushrooms', 'peppers')


### 结合使用位置实参和任意数量实参
### 如果要让函数接受不同类型的实参，必须在函数定义中将接纳任意数量实参的形参放置在最后
### Python 先匹配位置实参和关键字实参，再讲余下的实参都收集到最后一个形参中
p()
def make_pizza_size(size, *toppings):
    print('制作一个 ' + str(size) + ' 寸的 pizza 所需要的材料有：')
    for x in toppings:
        print('- ' + x)

make_pizza_size(10, '芝士', '牛杂', '猪肝')


### 使用任意数量的「关键字」实参，用【**parame】实现，放在所有形参的最后位置
### 调用函数的时候需要在最后输入任意数量的【键-值】对作为参数
### 案例：接收用户信息，除了姓名、年龄外，还有其他不确定的资料，生成用户的字典
p()
def build_profile(name, age, **more_info):
    profile = {}
    profile['name'] = name
    profile['age'] = 23
    for key, value in more_info.items():
        profile[key] = value
    return profile

dashen = build_profile('Ben', 27, location='Guangzhou', field='Python')
print(dashen)
