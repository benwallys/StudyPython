# -- coding: utf-8 --
# while 循环

# 当条件满足时，继续循环；当条件不满足时，退出循环，且不执行循环体内的代码
current = 1
while current <= 5:
	if current % 3 == 0:
		print(current)
	current += 1


# 和用户玩游戏，用户输入 quit 才能退出循环
message = ''
tip = "\nTell me something, and I will repeat it back to you:"
tip += "\nOr enter 'quit' to end the program. "

while message != 'quit':
	message = input(tip)
	if message != 'quit':
		print('Hello ' + message)
	else:
		print('see you next time')
print('normal method finished')


# 循环中想要退出循环，有两种方法
# 方法一：设置一个活动标志，设置标志为 True 可以使循环一直执行
# 当满足某个条件时，设置标志为 False，使循环条件不成立从而退出循环
sign = True
while sign:
	message = input(tip)
	if message == 'quit':
		sign = False
	else:
		print(message + ' sign is still TRUE')
print('sign method finished')

# 方法二：使用 break 方法，当满足某个条件时，break 退出循环
while True:
	message = input(tip)
	if message == 'quit':
		break
	else:
		print(message + ' break method not activity')
print('break method finished')


# 使用 continue 跳过本次的循环，进入下一次，注意要控制循环截止条件
i = 0
while i <= 10:
	i += 1
	if i % 3 == 1:
		continue
	print()
# 2
# 3
# 5
# 6
# 8
# 9
# 11