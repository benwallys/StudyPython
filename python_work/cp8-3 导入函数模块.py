# -- coding: utf-8 --
# 将函数存储在【模块】中，并在其他文件中导入使用

## 所有的 import 语句都应放在文件非注释语句的第一行

##################################################
#
# 对于运行文件与模块文件在同一目录下，可采用以下方法导入
#
##################################################

## 1. 导入整个模块，语法是【import module_name】
## 调用函数方法需要使用句点号，即【module_name.function_name()】

## 2. 导入模块中的某些函数，语法是【from module_name import function_name】
## 调用函数方法直接使用函数名即可，即【function_name()】

## 3. 使用 as 给函数制定别名，语法是【from module_name import function_name as fn】
## 调用函数方法直接使用别名即可，即【fn()】

## 4. 使用 as 给模块制定别名，语法是【import module_name as mn】
## 调用模块中的函数的时候，需要用模块别名加句点号加函数名，即【mn.function_name()】

## 5. 导入模块中的所有函数，语法是【from module_name import *】
## 调用函数方法直接使用函数名即可，即【function_name()】
## 注意：并非自己编写的大型模块，最好不要这样导入模块，因为可能会遇到重名的函数



##################################################
#
# 对于运行文件与模块文件所在的文件夹在同一目录下，可采用以下方法导入
#
##################################################

## 1. 需要在模块文件所在的目录下建立一个空文件，命名为 __init__.py
## 2. 导入整个模块，语法是【from 目录名 import module_name】
## 3. 调用函数时，语法是【module_name.function_name()】
from functions import cp8use
addNum = cp8use.add_num(3, 4)
cp8use.p(addNum)
plusNum = cp8use.plus_num(3, 5)
cp8use.p(plusNum)
squNum = cp8use.squ_num(6)
cp8use.p(squNum)
