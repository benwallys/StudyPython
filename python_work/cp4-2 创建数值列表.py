# -- coding: utf-8 --
# 操作列表

# 创建数值列表
# 函数 range() 可以轻松胜出一系列的数字
# 注意 range(x, y, z) 生成的是大于等于 x，小于 y 的数字；z 为增长长度，默认为 1
for v in range(1, 5):
	print(v)
for va in range(20, 40, 5):
	print(va)
print('\n')

# 使用函数 list() 将 range() 的结果直接转换为列表
numbers = list(range(1, 6))
print(numbers)
print('\n')

# 简单统计
print(min(numbers))
print(max(numbers))
print(sum(numbers))
print('\n')

# 一个吊炸天的语法——列表解析
squares = [value**2 for value in range(1, 11)]
print(squares)
# 上面的语法等同于下面的代码
sqs = []
for val in range(1, 11):
	sq = val**2
	sqs.append(sq)
print(sqs)