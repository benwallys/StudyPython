# -- coding: utf-8 --
# 模块中可以存储 n 个类
class Car():
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year

    def show_the_car(self):
        long_name = str(self.year) + " " + self.make + " " + self.model
        print('\nThis is ' + long_name.title())

class Battery():
    def __init__(self, battery=80):
        super(Battery, self).__init__()
        self.battery = battery


class ElectricCar(Car):
    def __init__(self, make, model, year):
        super().__init__(make, model, year)
        self.dianchi = Battery()

    def show_the_battery(self):
        print('The battery of the ' + self.model.title() + ' is ' + str(self.dianchi.battery))

# 模块与模块之间也可以相互导入
# 例如假如有另外一个模块在相同目录需要导入，只需要直接导入即可
# from module_name import class_name
