# -- coding: utf-8 --
# 操作列表

# 元组
# Python 将不能修改的值成为【不可变的】，而不可变的列表被成为【元组】
# 元组使用圆括号而不是方括号来标识
dimensions = (200, 50, 79)
print(dimensions[0])
print(dimensions[1])

# 遍历元组中的所有值
print('\n')
for di in dimensions:
	print(di)

# 元组内的元素不能改变，如果需要改变，只能整个元组重新定义
print('\n')
dimensions = (300, 20, 66)
for dim in dimensions:
	print(dim)