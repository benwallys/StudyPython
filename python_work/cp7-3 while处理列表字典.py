# -- coding: utf-8 --
# 使用 while 循环来处理列表和字典
# 原本 for 循环是一种遍历列表的有效方式，但在 for 循环中不应该修改列表，否则将导致
# Python 难以跟踪其元素。要在遍历列表的同时对其进行修改，可使用 while 循环。

# 案例一：在列表间移动元素
unconfirm_users = ['wiki', 'cobe', 'simon']
confirmed_users = []

while unconfirm_users:
	current_user = unconfirm_users.pop()

	print('Verifying user: ' + current_user.title())
	confirmed_users.append(current_user)

print('\nThe following users have been confirmed: ')
for x in confirmed_users:
	print(x.title())

# 案例二：删除包含特定值的所有列表元素
print('\n')
pets = ['cat', 'dog', 'bear', 'cat', 'fish', 'dog', 'rabbit']
print(pets)
while 'cat' in pets:
	pets.remove('cat')
print(pets)

# 案例三：使用用户的输入来填充字典
print('\n')
dream_place = {}
status = True
while status:
    name = input('What is your name? ')
    place = input('Where do you want to visit? ')
    dream_place[name] = place

    another = input('Any other want to say?(yes/no) ')
    if another == 'no':
        status = False

for name,place in dream_place.items():
    print(name.title() + "'s dream place is " + place.title())
