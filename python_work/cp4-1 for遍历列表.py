# -- coding: utf-8 --
# 操作列表

# 遍历列表
# 注意 for 那行最后要加冒号
# 循环体内的语句要进行缩进，如果不缩进，语法不会出错，但是逻辑会出错
# 循环体外的语句不必要进行缩进
mList = ['audio', 'benifit', 'cannon', 'dove']
for i in mList:
	print(i.title() + ' is a good thing')
	print('nice to meet ' + i.upper() + '\n')

print('good bye')
