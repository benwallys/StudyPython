# -- coding: utf-8 --
# 用户输入

# 注：用 sublime text 工具编译（command+B) input 代码的时候是无法运行的
# 所以需要在终端上运行本段代码

# 把提示语放到 input 函数中
msg = input('tell me something: ')
print(msg + '\n')

# 当提示语较长，需要多行展示的时候，可以把提示语存到一个变量中，再 input 变量
tip = 'If you tell us who you are, we can personalize the messages you see.'
tip += "\nWhat is your first name? "
name = input(tip)
print('hello ' + name.title() + '!')

# input 会将用户输入的内容存储为字符串，即使是数字
sth = input('how old are you? ')
print(sth > 18)
### 运行上面这句系统会有以下错误提示：
### TypeError: '>' not supported between instances of 'str' and 'int'
### 因此，如果是要对用户输入的内容变成数字，需要使用 int() 函数：
### print(int(sth) > 18)