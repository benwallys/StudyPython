# -- coding: utf-8 --
# 函数


# 无参数，直接执行功能的自定义函数
def sayHi():
    print('hi')

sayHi()


# 带参数，直接执行功能的自定义函数
### 定义函数时的 name 叫做形参，调用函数时输入的内容 ‘Steven’ 叫做实参
def sayHi2sb(name):
    print('hi ' + name.title())

sayHi2sb('steven')


### 传递实参
### 案例一：必填实参
print('\n')
def myPet(petType, petName):
    print('my pet is ' + petType + ', its name is ' + petName)

myPet('dog', 'jerry')
myPet(petName = 'tom', petType = 'cat')


### 默认值
### 案例二：非必填实参
print('\n')
def myToy(toyType='car', toyPrice=3):
    print('my toy is a ' + toyType + ', it costs me ' + str(toyPrice) + ' dollars')

myToy()
myToy('ship', 2.5)


# 返回值，黑盒——输入某物然后输出某物
### 案例一：返回内容
print('\n')
def getFullName(firstName, lastName, middleName=''):
    fullName = firstName.title() + ' ' + middleName.title() + ' ' + lastName.title()
    return fullName
print('my name is ' + getFullName('tom', 'cross', 'disk'))

### 案例二：计算器，输入参数，返回结果
print('\n')
def addNum(a = 3, b = 2):
    return int(a) + int(b)
print([addNum(4, 5), addNum(), addNum(33, 44)])

### 案例三：返回的内容是字典 —— 建立一个函数，返回人名信息字典
print('\n')
def buildPerson(firstName, lastName, age=''):
    person = {'first': firstName, 'last': lastName}
    if age:
        person['age'] = int(age)
    return person
print(buildPerson('tom', 'pick', 22))

### 传递的参数是列表
print('\n')
def greet_users(names):
    for name in names:
        print('Good morning, ' + name.title())

users = ['jerry', 'simon', 'cobe', 'wiki']
greet_users(users)
