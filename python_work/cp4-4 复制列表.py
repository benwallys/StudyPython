# -- coding: utf-8 --
# 操作列表

# 复制列表
my_foods = ['pizza', 'falafel', 'carrot cake']
print('my foods are: ')
print(my_foods)
# 复制一个新的列表，通过上一个列表提取一个包含所有元素的切片，来创建副本
her_foods = my_foods[:]
print('\nher foods are:')
print(her_foods)
# 通过复制将会得到两个不同的列表，可以尝试分别添加元素，两个列表最终会不一样
my_foods.append('apple')
her_foods.append('banana')
print('\nmy new foods are: ')
print(my_foods)
print('\nher new foods are:')
print(her_foods)

# python 不像 php，如果是直接将一个变量内容赋予另一个变量 her_foods = my_foods
# 将会得到的结果还是一个列表，只是有两个变量名称而已，看下方例子
my_toys = ['car', 'plane', 'train']
his_toys = my_toys
print('\nmy toys are:')
print(my_toys)
print('\nhis toys are:')
print(his_toys)
my_toys.append('bike')
print('\nmy new toys are:')
print(my_toys)
his_toys.append('ball')
print('\nhis new toys are:')
print(his_toys)