# -- coding: utf-8 --
# 操作列表

# 使用列表的一部分——切片
# 处理列表的部分元素，Python称之为【切片】
players = ['charles', 'martina', 'michael', 'florence', 'eli']

# 打印列表中大于等于第一个索引，小于第二个索引的所有元素
print(players[0:3])
print(players[2:4])

# 如果没有指定第一个索引，将自动从列表开头开始
print(players[:3])

# 如果没有指定第二个索引，将提取从第一个索引到列表末尾的所有的元素
print(players[2:])

# 要打印最后 n 个元素，可以这样
print(players[-3:])

# 遍历切片
# 处理数据时，可使用切片来进行批量处理
# 编写 Web 应用程序时，可使用切片来分页显示信息，并在每页显示数量合适的信息
# 例如要遍历前 3 名选手
print('\n')
for player in players[:3]:
	print(player.title())