# -- coding: utf-8 --
# 条件 if 语句
cars = ['audi', 'bmw', 'toyota', 'geely']
for car in cars:
	if car == 'bmw':
		print(car.upper())
	else:
		print(car.title())

if 'bmw' in cars:
	print('good')

# 一些基本的比较与检查
print('\n')
print('bmw' == 'bmw') # True
print(1 == 2) # False
print(32 > 33) # False
print(32 > 33 or 32 > 22) # True
print(15 <= 15 and 18 < 28) # True
print('bmw' in cars) # True
print('byd' in cars) # False

# if-else / if-elif-else
age = 22
if age < 18:
	print('\nyou are not old enough')
else:
	print('\nyou are old haha')

if age < 4:
	price = 0
elif age < 10:
	price = 5
else:
	price = 10
print('your ticket cost is $' + str(price) + '.\n')

# 确定列表不是空的
request_toppings = ['meet', 'cheese']
request_toppings = []
if request_toppings:
	for request_topping in request_toppings:
		print('Adding ' + request_topping + '.')
	print('Finished making your pizza')
else:
	print('Are you sure you want a plain pizza?')