# -- coding: utf-8 --
# 字典(参考 js 的对象)
# Python中的字典是一系列【键-值】对
alien = {'color': 'green', 'points': 5}
print(alien['color'])
print(alien['points'])

# 添加键值对
alien['x_pos'] = 25
alien['y_pos'] = 10
print(alien)

# 修改键的值
alien['color'] = 'yellow'
print(alien)

# 删除键值对
del alien['points']
print(alien)

# 遍历字典
for k,v in alien.items():
	print('\nKey: ' + k)
	print('Value: ' + str(v))

# 遍历字典中的所有键
print('\n')
for fun in alien.keys():
	print('keys are: ' + fun.title())

# 遍历字典中的所有值
print('\n')
for love in alien.values():
	print('values are: ' + str(love))

# 如果字典中的值有很多重复的，为了提出重复项，可使用集合 set 来过滤
# 集合 set 类似于列表，但每个元素都必须是唯一的
print('\n')
alien['first'] = 50
alien['second'] = 50
alien['third'] = 50
print(alien)
for passion in set(alien.values()):
	print(str(passion))