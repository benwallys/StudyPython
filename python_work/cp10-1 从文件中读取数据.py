# -- coding: utf-8 --
# 从文件中读取数据
with open('files/pi_digits.txt') as file_object:
    contents = file_object.read()
    # read() 到达文件末尾时返回一个空字符串，而将这个空字符串显示出来时就是一个空行。
    # 要删除多出来的空行，可在 print 语句中使用 rstrip()
    print(contents.rstrip())

# 注意文件的相对路径，如果是同目录下，则没有路径，如 'pi_digits.txt'
# 如果是当前文件所在目录的下一级目录下，则需要加上下一级目录名称 'files/pi_digits.txt'
# 如果要用绝对路径，那就会很长很长……


## 逐行读取文件内容
file_name = 'files/pi_digits.txt'
with open(file_name) as file_object_2:
    for line in file_object_2:
        print(line.rstrip())


## 创建一个包含文件内各行的列表
## 使用关键字 with 时，open() 返回的文件对象只在 with 代码块内可用。
## 如果要在 with 代码块外访问文件的内容，可在 with 代码块内将文件的各行存储在一个列表中
## 并在 with 代码块外使用该列表：你可以立即处理文件的各个部分，也可推迟到程序后面再处理。
with open(file_name) as file_object_3:
    lines = file_object_3.readlines()

for line in lines:
    print(line.rstrip())


## 使用文件的内容
with open(file_name) as file_object_4:
    lines = file_object_4.readlines()

pi_str = ''
for line in lines:
    # 在变量 pi_str 存储的字符串中，包含原来位于每行左边的空格
    # 为删除这些空格，可使用 strip() 而不是 rstrip()
    pi_str += line.strip()

print(pi_str)
print('文件中 pi 值的长度为：' + str(len(pi_str)))


## 判断某个值是否在文件中
judges = [str(46263), str(53589)]
for judge in judges:
    print('\nthe judge number is: ' + judge)
    if judge in str(pi_str):
        print(judge + ' is in pi_str')
    else:
        print('put another number in judge')
