def dump(x):
	print(str(x))

for n in range(2, 15):
	for x in range(2, n):
		# dump('now n is: ' + str(n))
		# dump('now x is: ' + str(x))
		if n % x == 0:
			# print(n, 'equals', x, '*', n//x)
			break
	else:
		# loop fell through without finding a factor
		print(n, 'is a prime number')
