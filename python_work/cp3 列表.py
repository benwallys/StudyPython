# -- coding: utf-8 --
# 学习列表的相关知识

# 列表用方括号表示
bicycles = ['trek', 'cannondale', 'redline', 'specialized']
print('列表元素为：')
print(bicycles)

# 访问列表的第一个元素
print('\n列表第一个元素为：')
print(bicycles[0])

# 访问列表的最后一个元素
print('\n列表最后一个元素为：')
print(bicycles[-1])

# 使用 append() 方法在列表末尾添加元素
bicycles.append('ofo')
print('\n使用 append() 方法列表末尾添加元素后为：')
print(bicycles)

# 使用 insert() 方法在列表指定位置添加元素
bicycles.insert(0, 'didi')
print('\n使用 insert() 方法在列表第1位添加元素后为：')
print(bicycles)

# 使用 del 语句删除列表中的元素，是永久删除，且不再以任何方式使用该元素
del bicycles[-1]
print('\n列使用 del 语句删除最后一个元素：')
print(bicycles)

# 使用 pop() 方法删除列表中的元素，删除后的元素还能继续使用
first = bicycles.pop(0)
print('\n使用 pop() 方法删除的元素为：' + first)
print('删除后的列表为：')
print(bicycles)


# 使用函数 len() 获取列表的长度
length = len(bicycles)
print('\n使用函数 len() 获取列表的长度为：')
print(length)

# 使用 sorted() 方法对列表进行临时性按字母顺序排序
print('\n使用 sorted() 方法对列表进行临时性按字母顺序排序结果：')
print(sorted(bicycles))
print('再看原列表，排序没变：')
print(bicycles)


# 使用 sort() 方法对列表进行永久性按字母顺序排序
print('\n使用 sort() 方法对列表进行永久性按字母顺序排序结果：')
bicycles.sort()
print(bicycles)
print('再看原列表，排序回不去了：')
print(bicycles)

# 使用 sort() 方法的参数 reverse=True 按字母顺序相反的排列列表元素
print('\n使用 sort() 带 reverse=True 参数对列表进行永久性按反字母顺序排序结果：')
bicycles.sort(reverse=True)
print(bicycles)

# 使用 reverse() 方法永久性倒着打印列表，但是可以随时恢复回来，只需要再使用一次即可
print('\n使用 reverse() 方法永久性倒着打印列表：')
bicycles.reverse()
print(bicycles)
print('再用一次恢复原来的顺序：')
bicycles.reverse()
print(bicycles)