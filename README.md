StudyPython

# 《Python编程从入门到实践》

Python Crash Course

A Hands-On, Project-Based Introduction to Programming

#### 【美】Eric Matthes 著  袁国忠 译

#### 中国工信出版集团 人民邮电出版社

ISBN 978-7-115-42802-8

本课程案例基于 Python 3.7.1 编写

## Menu

Table of contents

Introduction

### PART I: Basics 基础知识

- Chapter 1: Getting Started 起步
- Chapter 2: Variables and Simple Data Types 变量和简单数据类型
- Chapter 3: Introducing Lists 列表简介
- Chapter 4: Working with Lists 操作列表
- Chapter 5: if Statements if语句
- Chapter 6: Dictionaries 字典
- Chapter 7: User Input and while Loops 用户输入和while循环
- Chapter 8: Functions 函数
- Chapter 9: Classes 类
- Chapter 10: Files and Exceptions 文件和异常
- Chapter 11: Testing Your Code 测试代码

### PART II: Projects 项目

Project 1: Alien Invasion 项目1 外星人入侵
- Chapter 12: A Ship that Fires Bullets 武装飞船
- Chapter 13: Aliens! 外星人
- Chapter 14: Scoring 计分

Project 2: Data Visualization 项目2 数据可视化
- Chapter 15: Generating Data 生成数据
- Chapter 16: Downloading Data 下载数据
- Chapter 17: Working with APIs 使用API

Project 3: Web Applications 项目3 Web应用程序
- Chapter 18: Getting Started with Django Django入门
- Chapter 19: User Accounts 用户账户
- Chapter 20: Styling and Deploying an App 样式和应用部署

### Afterword 后记

- Appendix A: Installing Python 安装Python
- Appendix B: Text Editors 文本编辑器
- Appendix C: Getting Help 寻求帮助
- Appendix D: Using Git for Version Control 使用Git进行版本控制
